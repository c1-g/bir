;;; bir-core.el --- Core functions of bir            -*- lexical-binding: t; -*-

;; Copyright (C) 2021 c1-g

;; Author: c1-g <char1iegordon@protonmail.com>
;; Keywords: internal

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:
(require 'org-roam)

;;; Customization

(defgroup bir nil
  "Bastardized Incremental Reading in Emacs."
  :group 'external)

(defcustom bir-directory (expand-file-name "bir/" org-roam-directory)
  "TODO"
  :type 'string
  :group 'bir)

(defcustom bir-source-property "SOURCE"
  "TODO"
  :type 'string
  :group 'bir)

(defcustom bir-article-property "ARTICLE"
  "TODO"
  :type 'string
  :group 'bir)

(defcustom bir-parent-property "PARENT"
  ""
  :type 'string
  :group 'bir)

(add-to-list 'org-fc-directories bir-directory)

(defun bir-get-article (&optional just-id)
  (let* ((article-prop (org-entry-get nil bir-article-property t t))
         (article-title (cond
                         (article-prop
                          (and (string-match org-link-bracket-re article-prop)
                               (substring (match-string 2 article-prop) nil -3)))
                         ((= (org-outline-level) 0)
                          (cadar (org-collect-keywords '("TITLE"))))
                         (t
                          (save-excursion (while (org-up-heading-safe))
                                          (or (org-entry-get nil "title")
                                              (nth 4 (org-heading-components)))))))
         (article-id (cond (article-prop
                            (and (string-match org-link-bracket-re article-prop)
                                 (substring (match-string 1 article-prop) 3)))
                           ((= (org-outline-level) 0)
                            (org-id-get-create))
                           (t
                            (save-excursion (while (org-up-heading-safe))
                                            (org-id-get-create))))))
    (if just-id
        article-id
      (or article-prop
          (org-link-make-string (concat "id:" article-id) article-title)))))

(provide 'bir-core)
;;; bir-core.el ends here
