;;; bir-split.el --- Split functions for BIR         -*- lexical-binding: t; -*-

;; Copyright (C) 2021  c1-g

;; Author: c1-g <char1iegordon@protonmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'org-fc)
(require 'bir-core)

(defun bir-split (&optional match scope &rest skip)
  (interactive "P")
  (let* ((article (bir-get-article))
         (refs (org-entry-get nil "ROAM_REFS" t t))
         (split-dir (file-name-as-directory (expand-file-name (bir-get-article t) bir-directory)))
         (total (how-many org-outline-regexp-bol (point-min) (point-max)))
         (filename-fmt (concat "%0" (number-to-string (length (number-to-string total))) "d.org"))
         (count 0)
         ids)
    (make-directory split-dir t)
    (org-map-entries (lambda ()
                       (let ((desc (nth 4 (org-heading-components)))
                             (level (org-current-level))
                             (id (org-id-get-create))
                             (start (point)) (end))
                         (org-entry-put nil bir-article-property article)
                         (when refs (org-entry-put nil "ROAM_REFS" refs))
                         (ignore-errors (org-fc-type-topic-init))
                         (setq end (or (outline-next-heading)
                                       (point-max)))
                         (append-to-file start end (expand-file-name (format filename-fmt count)
                                                                     split-dir))
                         (cl-incf count)
                         (delete-region start end)
                         (insert "#+TRANSCLUDE: " (org-make-link-string (concat "id:" id) desc)
                                 " :level " (number-to-string level) "\n\n")
                         (push id ids)))
                     match scope skip)
    (save-buffer)
    (org-id-update-id-locations
     (directory-files-recursively split-dir directory-files-no-dot-files-regexp) t)
    (nreverse ids)))

(provide 'bir-split)
;;; bir-split.el ends here
