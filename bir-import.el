;;; bir-import.el --- Importing facilities for BIR   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  c1-g

;; Author: c1-g <char1iegordon@protonmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(defun bir-import (url &optional html)
  "URL's web page content.
Content is processed with `eww-readable' and Pandoc.
This is a simplified version of `org-web-tools--url-as-readable-org'."
  (let* ((url (or url (org-web-tools--get-first-url)))
          (html (with-temp-buffer
                  (insert (org-file-contents (or html url)))
                  (condition-case nil
                      ;; Fix undecoded text
                      (decode-coding-region (point-min) (point-max) 'utf-8)
                    (coding-system-error nil))
                  (buffer-string)))
          (html (org-web-tools--sanitize-html html url))
          (readable (cdr (org-web-tools--eww-readable html)))
          (converted (org-web-tools--html-to-org-with-pandoc readable)))
    (with-temp-buffer
      (org-mode)
      (insert converted)
      (org-web-tools--demote-headings-below 1)
      (buffer-string))))

(defun bir-import-full (url &optional html)
  "URL's web page content.
Content is processed with Pandoc without reflowing with `eww-readable'.
This is a simplified version of `org-web-tools--url-as-readable-org'."
  (let* ((url (or url (org-web-tools--get-first-url)))
          (html (with-temp-buffer
                  (insert (org-file-contents (or html url)))
                  (condition-case nil
                      ;; Fix undecoded text
                      (decode-coding-region (point-min) (point-max) 'utf-8)
                    (coding-system-error nil))
                  (buffer-string)))
          (html (org-web-tools--sanitize-html html url))
          (converted (org-web-tools--html-to-org-with-pandoc html)))
    (with-temp-buffer
      (org-mode)
      (insert converted)
      (org-web-tools--demote-headings-below 1)
      (buffer-string))))

(provide 'bir-import)
;;; bir-import.el ends here
