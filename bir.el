;;; bir.el --- Incremental Reading for Emacs         -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Charlie Gordon

;; Author: Charlie Gordon <char1iegordon@protonmail.com>
;; Keywords: multimedia, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'bir-core)
(require 'bir-extract)
(require 'bir-split)
(require 'bir-mercy)
(require 'bir-import)


(provide 'bir)
;;; bir.el ends here
